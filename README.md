# README #

### Completion time ###

This project took much longer than I expected due to circumsetances unpermitting completition in the given timeframe. 
The approximate time would be over 10 hours, over the course of several days. This is largerly in part of constantly changing the mechanics of the game and, of course, bugs.
### What is this repository for? ###

* This project is to valid knowledge in minor areas of networking (concurrency and tcp connectivity)
* Version: 1.0

### Assumptions ###

* Primary assumption was to draw from Go's strength, in that attaining to a broad use of goroutines wherever possible. And not simply to be superficial, 
  but to perhaps scale to a larger map size without incurring significant load time.
  
* Secondary assumption was how the game should be developed. While a simpler version of this could be accomplished fairly easily, I decided to make this a showcase
  of backend inclination. 