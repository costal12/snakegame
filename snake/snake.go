// Package snake maintains snake functions
package snake

import (
	"fmt"
	"sync"

	"bitbucket.com/costal/snakegame/helper"
)

type snakePart struct {
	Position    helper.Position
	oldPosition helper.Position
	readier     chan struct{}
}

type Body struct {
	body []*snakePart
	Head *snakePart
}

func New() *Body {
	return &Body{body: make([]*snakePart, 0)}
}

func newSnakePart(x, y int) *snakePart {
	return &snakePart{
		Position:    helper.Position{X: x, Y: y},
		oldPosition: helper.Position{X: x, Y: y},
		readier:     make(chan struct{}),
	}
}

func (s *Body) Size() int { return len(s.body) }

func (s *Body) EatAtPoint(x, y int) *Body {
	s.body = append(s.body, newSnakePart(x, y))
	s.Head = s.body[len(s.body)-1]
	return s
}

func (s *Body) MoveTo(x, y int, before, after func(int, int)) {
	p := helper.Position{X: x, Y: y}
	fmt.Printf("Moving from %v to %v\n", s.Head.Position, p)
	s.Head.Position = p
	if len(s.body) > 1 {
		var wg sync.WaitGroup
		helper.AsyncLoop(0, len(s.body)-1, func(mid int) {
			wg.Add(1)
			com := make(chan struct{})
			// Receiver
			go func() {
				if mid > 0 {
					<-s.body[mid].readier
				}
				<-com
				before(
					s.body[mid].oldPosition.X,
					s.body[mid].oldPosition.Y,
				)
				s.body[mid].oldPosition = s.body[mid].Position
				com <- struct{}{}
			}()

			// Accessor
			go func() {
				if mid < len(s.body)-1 {
					s.body[mid].Position = s.body[mid+1].oldPosition
					s.body[mid+1].readier <- struct{}{}
				}
				com <- struct{}{}
				<-com
				wg.Done()
			}()
		})
		wg.Wait()
		for _, val := range s.body {
			after(
				val.Position.X,
				val.Position.Y,
			)
		}
	} else {
		before(
			s.Head.oldPosition.X,
			s.Head.oldPosition.Y,
		)
		after(
			s.Head.Position.X,
			s.Head.Position.Y,
		)
		s.Head.oldPosition = s.Head.Position
	}
}