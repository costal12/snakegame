package helper

type Position struct{ X, Y int }

func (p Position) Add(otherP Position) Position {
	p.X += otherP.X
	p.Y += otherP.Y
	return p
}

func (p Position) IsInBounds(otherP Position, distX, distY int) bool {
	return otherP.X <= (p.X-distX) && otherP.X >= distX &&
		otherP.Y <= (p.Y-distY) && otherP.Y >= distY
}