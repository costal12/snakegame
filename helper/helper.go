// Package helper contains helper functions and types
package helper

import "sync"

func AsyncLoop(low, high int, f func(i int)) {
	if low <= high {
		var wg sync.WaitGroup
		wg.Add(2)
		mid := (low + high) / 2
		go func() {
			defer wg.Done()
			AsyncLoop(low, mid-1, f)
		}()
		go func() {
			defer wg.Done()
			AsyncLoop(mid+1, high, f)
		}()
		f(mid)
		wg.Wait()
	}
}