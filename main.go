package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"

	"bitbucket.com/costal/snakegame/board"
)

func main() {
	w := flag.Int("width", 10, "set board width")
	h := flag.Int("height", 10, "set board height")
	flag.Parse()

	board := board.New(*w, *h)
	board.Populate()

	welcomeMessage := `
*********************************
*********** S N A K E ***********
*********************************
---------------------------------
Directions: use WSAD keys to move
            type e to exit game
`
	fmt.Print(welcomeMessage)
	buf := bufio.NewReader(os.Stdin)
	for board.GameOn {
		board.Draw()
		input, _ := buf.ReadString('\n')
		key := string([]byte(input)[0])
		switch key {
		case "w":
			board.MoveSnakeUp()
		case "s":
			board.MoveSnakeDown()
		case "a":
			board.MoveSnakeLeft()
		case "d":
			board.MoveSnakeRight()
		case "e":
			board.GameOn = false
		}
	}
	board.GameOver()
}