// Package board maintains board functions
package board

import (
	"fmt"
	"math/rand"
	"sync"
	"time"

	"bitbucket.com/costal/snakegame/helper"
	"bitbucket.com/costal/snakegame/snake"
)

type field struct {
	Width            int
	Height           int
	GameOn           bool
	board            [][]int
	graphics         [][]byte
	snake            *snake.Body
	snakeOnBoard     bool
	loadLimit        float32
	reorderThreshold float32
	hasReordered     []bool
	availableSpots   *sync.Map
}

func New(x, y int) *field {
	mArray := make([][]int, y)
	gArray := make([][]byte, y+2)
	spotMap := sync.Map{}
	for i := range mArray {
		mArray[i] = make([]int, x)
		spotMap.Store(i, x)
	}
	for i := range gArray {
		gArray[i] = make([]byte, x+2)
	}
	board := &field{
		Width:            x,
		Height:           y,
		GameOn:           true,
		board:            mArray,
		graphics:         gArray,
		snake:            snake.New(),
		loadLimit:        0.4,
		reorderThreshold: 0.5,
		hasReordered:     make([]bool, x),
		availableSpots:   &spotMap,
	}
	return board
}

func (b *field) setGraphical() {
	/*
			   |================|
			   |                |
		         |                |
			   |                |
			   |================|
	*/
	l := len(b.graphics)
	helper.AsyncLoop(0, l-1, func(i int) {
		w := len(b.graphics[i])
		b.graphics[i][0] = '|'
		b.graphics[i][w-1] = '|'
		helper.AsyncLoop(0, w-1, func(j int) {
			if j > 0 && j < w-1 {
				if i == 0 || i == l-1 {
					b.graphics[i][j] = '='
				} else {
					b.graphics[i][j] = ' '
				}
			}
		})
	})
}

func (b *field) Populate() {
	if !b.snakeOnBoard {
		b.setGraphical()
		xRand := rand.New(rand.NewSource(time.Now().UnixNano()))
		yRand := rand.New(rand.NewSource(time.Now().UnixNano()))
		x := xRand.Intn(b.Width)
		y := yRand.Intn(b.Height)
		b.positionInSpot(x, y, 2)
		b.snake.EatAtPoint(x, y)
		b.snakeOnBoard = true
	}
	b.GameOn = b.spawnTarget()
}

func (b *field) MoveSnakeUp() {
	if b.inBounds(b.snake.Head.Position.Add(helper.Position{X: 0, Y: -1})) {
		b.moveSnakeByPoint(0, -1)
	}
}

func (b *field) MoveSnakeDown() {
	if b.inBounds(b.snake.Head.Position.Add(helper.Position{X: 0, Y: 1})) {
		b.moveSnakeByPoint(0, 1)
	}
}

func (b *field) MoveSnakeLeft() {
	if b.inBounds(b.snake.Head.Position.Add(helper.Position{X: -1, Y: 0})) {
		b.moveSnakeByPoint(-1, 0)
	}
}

func (b *field) MoveSnakeRight() {
	if b.inBounds(b.snake.Head.Position.Add(helper.Position{X: 1, Y: 0})) {
		b.moveSnakeByPoint(1, 0)
	}
}
func (b *field) inBounds(position helper.Position) bool {
	b.GameOn = helper.Position{X: b.Width - 1, Y: b.Height - 1}.IsInBounds(position, 0, 0)
	return b.GameOn
}

func (b *field) moveSnakeByPoint(x, y int) {
	p := b.snake.Head.Position.Add(helper.Position{X: x, Y: y})
	if b.board[p.Y][p.X] == 1 {
		b.snake.EatAtPoint(p.X, p.Y)
		b.graphics[p.Y+1][p.X+1] = 'o'
		b.board[p.Y][p.X] = 2
		b.Populate()
	} else {
		b.snake.MoveTo(p.X, p.Y,
			func(prevX, prevY int) {
				positionLocker.Lock()
				b.releaseInSpot(prevX, prevY)
				positionLocker.Unlock()
			}, func(newX, newY int) {
				positionLocker.Lock()
				b.GameOn = b.positionInSpot(newX, newY, 2)
				positionLocker.Unlock()
			})
	}
}

func (b *field) Draw() {
	for _, line := range b.graphics {
		fmt.Println(string(line))
	}
}

func (b *field) SetLoadLimit(limit float32) {
	b.loadLimit = limit
}

func (b *field) SetReorderThreshold(threshold float32) {
	b.reorderThreshold = threshold
}

func (b *field) spawnTarget() bool {
	ok := false
	helper.AsyncLoop(0, len(b.board)-1, func(i int) {
		trycount := 0
		for spots, _ := b.availableSpots.Load(i); spots.(int) > 0 && !ok; {
			availableSpots := spots.(int)
			boardLine := b.board[i]
			spotsFactor := float32(availableSpots) / float32(len(b.board[i]))
			blLength := len(boardLine)
			if !b.hasReordered[i] && spotsFactor <= (b.reorderThreshold*b.loadLimit) {
				var locker sync.Mutex
				var n int
				helper.AsyncLoop(0, blLength-1, func(j int) {
					locker.Lock()
					if boardLine[j] == 0 {
						boardLine[n] = 0
						n++
					}
					locker.Unlock()
				})
				b.hasReordered[i] = true
			}
			if b.hasReordered[i] {
				boardLine = b.board[i][:availableSpots]
				blLength = len(boardLine)
			}
			rand.Seed(time.Now().UnixNano())
			spot := b.scalePosition(rand.Intn(blLength), blLength)
			positionLocker.Lock()
			if !ok {
				ok = b.positionInSpot(spot, i, 1)
			}
			positionLocker.Unlock()
			if (1-spotsFactor) >= b.loadLimit && trycount == 3 {
				shuffleRand := rand.New(rand.NewSource(time.Now().UnixNano()))
				shuffleRand.Shuffle(blLength, func(i, j int) {
					boardLine[i], boardLine[j] = boardLine[j], boardLine[i]
				})
				trycount = 0
			}
			trycount += 1
		}
	})
	return ok
}

func (b *field) scalePosition(i, l int) int {
	return int((float32(i) / float32(l)) * float32(len(b.board[0])))
}

var positionLocker sync.Mutex

func (b *field) positionInSpot(x, y, id int) bool {
	val, _ := b.availableSpots.Load(y)
	spots := val.(int)
	if b.board[y][x] == 0 && ((id == 1 && spots > 0) || id == 2) {
		b.board[y][x] = id
		b.availableSpots.Store(y, spots-1)
		b.setGraphicalID(x, y, id)
		return true
	}
	return false
}

func (b *field) releaseInSpot(x, y int) bool {
	val, _ := b.availableSpots.Load(y)
	spots := val.(int)
	if b.board[y][x] > 0 {
		b.board[y][x] = 0
		if spots < len(b.board[y]) {
			b.availableSpots.Store(y, spots+1)
		}
		b.setGraphicalID(x, y, 0)
		return true
	}
	return false
}

func (b *field) setGraphicalID(x, y, id int) {
	switch id {
	case 1:
		b.graphics[y+1][x+1] = '.'
	case 2:
		b.graphics[y+1][x+1] = 'o'
	default:
		b.graphics[y+1][x+1] = ' '
	}
}

func (b *field) GameOver() {
	fmt.Println("GAME OVER! You made it to round ", b.snake.Size()-1)
}